--[[-------------------------------------------------------------------------
	Добавление опций для переназначения клавиш по массиву в Q меню
-------------------------------------------------------------------------]]

-- Создаем таблицу с настройками по умолчанию
local keySettings = {
    [KEY_F1] = "xgui toggle",
    [KEY_F2] = "ppm2_editor3",
    [KEY_F3] = "outfitter_open",
    [KEY_F4] = "pac_editor"
}

-- Функция для изменения настроек клавиш
local function KeySettings(panel)
    for key, command in pairs(keySettings) do
        local ctrl = vgui.Create("DTextEntry", panel)
        ctrl:SetValue(command)
        ctrl:SetSize(200, 20)
        ctrl.key = key
        ctrl.OnEnter = function()
            keySettings[key] = ctrl:GetValue()
        end
        panel:AddItem(ctrl)
    end
end

-- Создаем пункт меню с настройками клавиш
spawnmenu.AddToolMenuOption("Оформление", "Функциональные клавиши", "Переназначение клавиш", "Custom Key Bindings", "", "", KeySettings)

--[[-------------------------------------------------------------------------
	Переназначение функциональных клавиш (F1-F4)
-------------------------------------------------------------------------]]
--

-- Функция для выполнения команды по нажатию клавиши
function ExecuteKeyCommand(ply, key)
    local command = keySettings[key]
    if command then
        ply:ConCommand(command)
    end
end

-- Меняем функции GM:ShowHelp, GM:ShowTeam и другие на использование функции ExecuteKeyCommand
function GM:ShowHelp(ply)
    if IsValid(ply) then
        ExecuteKeyCommand(ply, KEY_F1)
    end
end

function GM:ShowTeam(ply)
    if IsValid(ply) then
        ExecuteKeyCommand(ply, KEY_F2)
    end
end

function GM:ShowSpare1(ply)
    if IsValid(ply) then
        ExecuteKeyCommand(ply, KEY_F3)
    end
end

function GM:ShowSpare2(ply)
    if IsValid(ply) then
        ExecuteKeyCommand(ply, KEY_F4)
    end
end
