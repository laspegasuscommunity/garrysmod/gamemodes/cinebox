 --[[-------------------------------------------------------------------------
	Функция Cinema, позволяющая скрыть курсор
-------------------------------------------------------------------------]]
--

GM.MouseEnabled = false

function GM:ShowMouse()
    if self.MouseEnabled then return end
    gui.EnableScreenClicker(true)
    RestoreCursorPosition()
    self.MouseEnabled = true
end

function GM:HideMouse()
    if not self.MouseEnabled then return end
    RememberCursorPosition()
    gui.EnableScreenClicker(false)
    self.MouseEnabled = false
end

--[[-------------------------------------------------------------------------
	Функция, позволяющая запускать меню из режима Cinema
-------------------------------------------------------------------------]]
--

-- Инициализация переменной для хранения местоположения игрока
local playerLocation = nil

-- Прослушивание момента, как игрок входит в театр
hook.Add( "PlayerEnterTheater", "UpdatePlayerLocation", function( ply, theater )
    -- Установка местоположения игрока внутри театра
    playerLocation = "theater"
end )

-- Прослушивание момента, как игрок выходит из театра
hook.Add( "PlayerLeaveTheater", "UpdatePlayerLocation", function( ply, theater )
    -- Установка местоположения игрока снаружи театра
    playerLocation = nil
end )

-- Вызов соответствующих функций контекстного меню в зависимости от местоположения игрока
function GM:OnContextMenuOpen()
    if playerLocation == "theater" then
        if not IsValid(LocalPlayer()) or not LocalPlayer().GetTheater then return end
        local Theater = LocalPlayer():GetTheater()
        if not Theater then return end

        if not IsValid(GuiQueue) then
            GuiQueue = vgui.Create("ScoreboardQueue")
        end

        GuiQueue:InvalidateLayout()
        GuiQueue:SetVisible(true)
        GAMEMODE:ShowMouse()

        if LocalPlayer():IsAdmin() or (Theater:IsPrivate() and Theater:GetOwner() == LocalPlayer()) then
            if not IsValid(GuiAdmin) then
                GuiAdmin = vgui.Create("ScoreboardAdmin")
            end

            GuiAdmin:InvalidateLayout()
            GuiAdmin:SetVisible(true)
        end
    else
        if IsValid(g_ContextMenu) and not g_ContextMenu:IsVisible() then
            g_ContextMenu:Open()
            menubar.ParentTo(g_ContextMenu)
        end
        hook.Call("ContextMenuOpened", self)
    end
end

function GM:OnContextMenuClose()
    if playerLocation == "theater" then
        if IsValid(GuiQueue) then
            GuiQueue:SetVisible(false)
        end
        if IsValid(GuiAdmin) then
            GuiAdmin:SetVisible(false)
        end
        if not (IsValid(Gui) and Gui:IsVisible()) then
            GAMEMODE:HideMouse()
        end
    else
        if IsValid(g_ContextMenu) then
            g_ContextMenu:Close()
        end
        hook.Call("ContextMenuClosed", self)
    end
end

--[[-------------------------------------------------------------------------
	Функции меню кинотеатра
-------------------------------------------------------------------------]]
--
--[[-------------------------------------------------------------------------
	Функции меню TAB
-------------------------------------------------------------------------]]
--
